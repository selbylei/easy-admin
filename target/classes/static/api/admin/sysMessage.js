/**
 * 分页查询消息列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysMessage/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询消息详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysMessage/query/' + id,
        method: 'get'
    })
}

/**
 * 新增消息
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysMessage/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改消息
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysMessage/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除消息
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysMessage/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出消息
 *
 * @param query
 * @returns {*}
 */
function exportMessage(query) {
    return requests({
        url: '/admin/sysMessage/export',
        method: 'get',
        params: query
    })
}

package com.mars.module.tool.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.GenCreateTableRecord;
import com.mars.module.tool.entity.GenSysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 建表记录 数据层
 *
 * @author mars
 */
public interface GenCreateTableRecordMapper extends BasePlusMapper<GenCreateTableRecord> {

}

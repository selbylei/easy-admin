package com.mars.module.tool.mapper;

import com.mars.module.tool.entity.SysOperLog;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 操作日志记录Mapper接口
 *
 * @author mars
 * @date 2023-11-17
 */
public interface SysOperLogMapper extends BasePlusMapper<SysOperLog> {

}

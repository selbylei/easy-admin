package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysNotify;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.MsgRequest;
import com.mars.module.admin.request.SysNotifyRequest;

import java.util.List;

/**
 * 通知公告接口
 *
 * @author mars
 * @date 2023-12-06
 */
public interface ISysNotifyService {
    /**
     * 新增
     *
     * @param param param
     * @return SysNotify
     */
    SysNotify add(SysNotifyRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysNotifyRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysNotify
     */
    SysNotify getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysNotify>
     */
    PageInfo<SysNotify> pageList(SysNotifyRequest param);


    /**
     * 发送通知
     *
     * @param param param
     */
    void sendNotify(SysNotifyRequest param);


    /**
     * 发送消息
     *
     * @param request request
     */
    void sendMsg(MsgRequest request);

}

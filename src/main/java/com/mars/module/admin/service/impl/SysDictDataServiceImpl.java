package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import com.mars.common.util.StringUtil;
import com.mars.framework.exception.ServiceException;
import com.mars.module.admin.entity.SysDictType;
import com.mars.module.admin.mapper.SysDictTypeMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.SysDictDataRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.SysDictDataMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.SysDictData;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.ISysDictDataService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * 字典数据业务层处理
 *
 * @author mars
 * @date 2023-11-18
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysDictDataServiceImpl implements ISysDictDataService {

    private final SysDictDataMapper baseMapper;

    private final SysDictTypeMapper dictTypeMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysDictData add(SysDictDataRequest request) {
        // 查询字典类型ID
        Long dictTypeId = request.getDictTypeId();
        SysDictType sysDictType = dictTypeMapper.selectById(dictTypeId);
        if (Objects.isNull(sysDictType)) {
            throw new ServiceException("当前字典类型不存在");
        }
        SysDictData entity = new SysDictData();
        BeanUtils.copyProperties(request, entity);
        entity.setDictType(sysDictType.getType());
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(SysDictDataRequest request) {
        SysDictData entity = new SysDictData();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public SysDictData getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<SysDictData> pageList(SysDictDataRequest request) {
        Page<SysDictData> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysDictData> query = this.buildWrapper(request);
        IPage<SysDictData> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    @Override
    public List<SysDictData> getDictData(String type) {
        if (StringUtil.isEmpty(type)) {
            throw new ServiceException("类型不能为空");
        }
        return baseMapper.selectList(Wrappers.lambdaQuery(SysDictData.class)
                .eq(SysDictData::getDictType, type).orderByAsc(SysDictData::getDictSort));
    }

    private LambdaQueryWrapper<SysDictData> buildWrapper(SysDictDataRequest param) {
        LambdaQueryWrapper<SysDictData> query = new LambdaQueryWrapper<>();
        return query;
    }

}

package com.mars.common.request.tool;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author 程序员Mars
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "一键部署请求参数")
public class GenCodeDeployRequest {

    /**
     * 表名称
     */
    @ApiModelProperty(value = "表名称")
    private List<String> tableName;

    /**
     * 1 目录 2 菜单
     */
    @ApiModelProperty(value = " 1 目录 2 菜单")
    private Integer type;

    /**
     * 代码生成菜单目录id
     */
    @ApiModelProperty(value = "代码生成菜单目录id")
    private Long menuId;

}

package com.mars.common.request.sys;

import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-10-20 23:39:02
 */
@Data
public class ResourceInfoFetchRequest {

    /**
     * 采集类型
     */
    private Integer type;

    /**
     * 采集页数
     */
    private Integer page;

}

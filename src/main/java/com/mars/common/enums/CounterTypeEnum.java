package com.mars.common.enums;

/**
 * @author WANGYUTAO28
 * @crete 2021 - 05 - 21 11:35
 */
public enum CounterTypeEnum {


    PARA_ALL_PASS("单节点多实例并行全部通过"),
    PARA_ONE_PASS("单节点多实例并行一人通过"),
    SEQUENCE("单节点多实例串行"),
    NOT_COUNTER("非会签节点");

    private String msg;


    CounterTypeEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isParallel() {
        return (this == PARA_ALL_PASS) || (this == PARA_ONE_PASS);
    }

    public boolean isSequence() {
        return this == SEQUENCE;
    }

    public boolean notCounter() {
        return this == NOT_COUNTER;
    }

    public boolean isCounterNode() {
        return this != NOT_COUNTER;
    }
}

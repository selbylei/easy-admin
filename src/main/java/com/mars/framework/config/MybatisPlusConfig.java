package com.mars.framework.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.mars.framework.config.mybatsplus.SpecialCharacterConversionLikeInnerInterceptor;
import com.mars.framework.handler.CustomerMetaHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author: Mars
 * @create: 2023-11-03 11:28
 */
@Configuration
@EnableTransactionManagement
@ComponentScan("com.mars")
@MapperScan("com.mars.module.**.mapper")
public class MybatisPlusConfig {


    /**
     * 自动填充功能
     *
     * @return GlobalConfig
     */
    @Bean
    public GlobalConfig globalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setMetaObjectHandler(new CustomerMetaHandler());
        return globalConfig;
    }


    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 分页插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        // 乐观锁插件
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }


    @Bean
    public SpecialCharacterConversionLikeInnerInterceptor myInterceptor() {
        return new SpecialCharacterConversionLikeInnerInterceptor();
    }


}
